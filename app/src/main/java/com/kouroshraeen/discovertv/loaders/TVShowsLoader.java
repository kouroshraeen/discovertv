package com.kouroshraeen.discovertv.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.os.OperationCanceledException;

import com.kouroshraeen.discovertv.BuildConfig;
import com.kouroshraeen.discovertv.api.MovieDatabaseService;
import com.kouroshraeen.discovertv.api.RetrofitClient;
import com.kouroshraeen.discovertv.models.TVShow;
import com.kouroshraeen.discovertv.models.TVShowResult;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;

/**
 * Created by Kourosh on 6/4/2017.
 */

public class TVShowsLoader extends AsyncTaskLoader<List<TVShow>> {

    private List<TVShow> mShows;
    private List<TVShow> mLastShows;
    private int mPage;

    public TVShowsLoader(Context context, int page) {
        super(context);
        mPage = page;
    }

    @Override
    public List<TVShow> loadInBackground() {
        if (isLoadInBackgroundCanceled()) {
            throw new OperationCanceledException();
        }
        String page = String.valueOf(mPage);
        MovieDatabaseService service = RetrofitClient.getRetrofitClient();
        Call<TVShowResult> call = service.getTVShows(BuildConfig.THE_MOVIE_DB_API_KEY, page, "popularity.desc");

        try {
            TVShowResult mResult = call.execute().body();
            if (mResult != null) {
                mShows = mResult.getResults();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mShows;
    }

    @Override
    public void deliverResult(List<TVShow> data) {
        if (isReset()) {
            // Do any necessary cleanup
        }

        mLastShows = data;

        if (isStarted()) {
            super.deliverResult(data);
        }
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();

        if (mLastShows != null) {
            deliverResult(mLastShows);
        } else {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();

        cancelLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();

        onStopLoading();
    }
}
