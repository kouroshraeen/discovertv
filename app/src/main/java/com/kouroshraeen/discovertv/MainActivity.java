package com.kouroshraeen.discovertv;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.kouroshraeen.discovertv.adapters.TVShowsAdapter;
import com.kouroshraeen.discovertv.loaders.TVShowsLoader;
import com.kouroshraeen.discovertv.models.TVShow;

import java.util.List;

public class MainActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<List<TVShow>> {

    private RecyclerView mRecyclerView;
    private TVShowsAdapter mAdapter;
    private int mPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_tv_shows);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                mPage = page + 1;
                getSupportLoaderManager().restartLoader(0, null, MainActivity.this);
            }
        });

        mAdapter = new TVShowsAdapter(this, null);

        mRecyclerView.setAdapter(mAdapter);

        getSupportLoaderManager().initLoader(0, null, this);
    }

    @Override
    public Loader<List<TVShow>> onCreateLoader(int id, Bundle args) {
        return new TVShowsLoader(this, mPage);
    }

    @Override
    public void onLoadFinished(Loader<List<TVShow>> loader, List<TVShow> data) {
        mAdapter.setData(data);
    }

    @Override
    public void onLoaderReset(Loader<List<TVShow>> loader) {
        mAdapter.setData(null);
    }
}
