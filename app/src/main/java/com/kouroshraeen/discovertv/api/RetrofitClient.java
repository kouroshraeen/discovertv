package com.kouroshraeen.discovertv.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Kourosh on 6/3/2017.
 */

public class RetrofitClient {

    private static MovieDatabaseService apiService;

    public static MovieDatabaseService getRetrofitClient(){
        // Check to see if an OpenStatesApi is already instantiated
        if(apiService == null){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(MovieDatabaseService.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            apiService = retrofit.create(MovieDatabaseService.class);
        }
        return apiService;
    }
}
