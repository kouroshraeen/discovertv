package com.kouroshraeen.discovertv.api;

import com.kouroshraeen.discovertv.models.TVShowResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Kourosh on 6/3/2017.
 */

public interface MovieDatabaseService {

    public static final String BASE_URL = "http://api.themoviedb.org/3/";

    @GET("discover/tv")
    Call<TVShowResult> getTVShows(@Query("api_key") String apiKey, @Query("page") String page, @Query("sort_by") String sortBy);
}
