package com.kouroshraeen.discovertv.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kouroshraeen.discovertv.R;
import com.kouroshraeen.discovertv.models.TVShow;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Kourosh on 6/4/2017.
 */

public class TVShowsAdapter extends RecyclerView.Adapter<TVShowsAdapter.ViewHolder> {

    private static final String BASE_POSTER_URL = "http://image.tmdb.org/t/p/w185/";
    private List<TVShow> mShows;
    private Context mContext;

    public TVShowsAdapter(Context context, List<TVShow> shows) {
        mContext = context;
        mShows = shows;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.item_tv_show, parent, false);

        // Return a new holder instance
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TVShow tvShow = mShows.get(position);
        Uri builtUri = Uri.parse(BASE_POSTER_URL).buildUpon().appendEncodedPath(tvShow.getPosterPath()).build();

        Picasso.with(mContext)
                .load(builtUri.toString())
                .into(holder.poster);
    }

    @Override
    public int getItemCount() {
        if (mShows != null) {
            return mShows.size();
        } else {
            return 0;
        }
    }

    public void setData(List<TVShow> data) {
        if (null != data && !data.isEmpty()) {
            if (mShows == null || mShows.isEmpty()) {
                mShows = data;
            } else {
                mShows.addAll(data);
            }
            notifyDataSetChanged();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView poster;

        ViewHolder(View itemView) {
            super(itemView);

            poster = (ImageView) itemView.findViewById(R.id.tv_show_poster);
        }
    }
}
